# random_partners #

The script uses two comma-separated lists as input and generates random pairs. It first tries to include one element from each list, but if one list is longer than the other, remaining elements will be paired too. If the remaining elements is not an odd number, the random element from two lists will be selected for the second time.

## Requirements ##

This script requires R libraries:
* tidyverse
* data.table
* optparse

## Usage ##
Example (assuming that folders are in the current directory):

```unix
Rscript random_partners.R
        -m Ieva,Roza,Kata,Vipin,Jaime
        -k Ping-Han,Tanya,Romana
        -s 42
        -o random_partners
```

## Parameters ##

```unix
-m : (--list_one)         Comma-separated list of elements to pair with elements from another list. (Mandatory)
-k : (--list_two)         Comma-separated list of elements to pair with elements from another list. (Mandatory)
-s : (--seed)             The seed to randomize the lists. (Default is no seed.)
-o : (--output_folder)    The output folder. (Default is current directory.)
```

## Output ##

The output is a table exported in the current directory by default or in a preferred directory and is named:
```unix
random_partners.txt
```

It is a two-column table:

```unix
partner_1	  partner_2
Kata	      Ping-Han
Ieva	      Roza
Jaime	      Tanya
Vipin	      Romana
```

# README #

Last updated: 2021 October 21

This repository contains all kinds of scripts for various things.

### How to use the scripts in this repo? ###

Clone the git repo from BitBucket

```
git clone https://ievara@bitbucket.org/ievara/utilities.git

cd utilities
```

Bellow you can find more information on each script.

### Available scripts ###

#### Random partners ####

The script uses two comma-separated lists as input and generates random pairs. It first tries to include one element from each list, but if one list is longer than the other, remaining elements will be paired too. If the remaining elements is not an odd number, the random element from two lists will be selected for the second time.

Read more in the [script's folder](https://bitbucket.org/ievara/utilities/src/master/random_partners/) README.

#### NCMM Tuesday seminar feedback processor ####

The repository contains a Shiny app to process the feedback given to the presenters of [Center for Molecular Medicine Norway](https://www.med.uio.no/ncmm/english/) (NCMM) Tuesday seminars. The feedback is given by people who attended the seminar by answering [this Google form](https://forms.gle/P1SZi2HnYjBKX1Lz6). The answers are extracted from the Google sheets table for each speaker. This table is then saved and submitted into the Shiny app.

Read more in the [script's folder](https://bitbucket.org/ievara/utilities/src/master/biotuesday_feedback_processor/) README.

### Contact info ###

Repo was created by me - Ieva Rauluseviciute.

Contact me:

* E-mail:
  - ievarau@ncmm.uio.no
  - ieva.rauluseviciute@gmail.com

* Twitter:
  [@IevaRau](https://twitter.com/IevaRau)
# biotuesday_feedback_processor #

The repository contains a Shiny app to process the feedback given to the presenters of [Center for Molecular Medicine Norway](https://www.med.uio.no/ncmm/english/) (NCMM) Tuesday seminars. The feedback is given by people who attended the seminar by answering [this Google form](https://forms.gle/P1SZi2HnYjBKX1Lz6). The answers are extracted from the Google sheets table for each speaker. This table is then saved and submitted into the Shiny app.

## How to access the app? ##

The app can be accessed and used by clicking [https://ievarau.shinyapps.io/biotuesday_feedback_processor/](https://ievarau.shinyapps.io/biotuesday_feedback_processor/).

## Usage ##

* Users can choose the input file and specify whether the file had a header.
* Users can choose what was the separator used to separate the column values. Recommended is **"Tab"**.
* Users can choose the quating style. Recommended is **"Double Quote"**.
